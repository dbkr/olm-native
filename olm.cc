/** \brief native binding for Olm
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "account.hh"
#include "session.hh"
#include "utility.hh"
#include "inbound_group_session.hh"
#include "outbound_group_session.hh"
#include "util.hh"

namespace olm_nan {
  NAN_METHOD(get_library_version) {
    uint8_t major, minor, patch;
    olm_get_library_version(&major, &minor, &patch);
    auto rv = Nan::New<v8::Array>();
    Nan::Set(rv, 0, Nan::New(major));
    Nan::Set(rv, 1, Nan::New(minor));
    Nan::Set(rv, 2, Nan::New(patch));
    RETURN(rv);
  }
}

NAN_MODULE_INIT(InitAll) {
  olm_nan::AccountWrap::Init(target);
  olm_nan::SessionWrap::Init(target);
  olm_nan::UtilityWrap::Init(target);
  olm_nan::InboundGroupSessionWrap::Init(target);
  olm_nan::OutboundGroupSessionWrap::Init(target);
  Nan::SetMethod(target, "get_library_version", olm_nan::get_library_version);
}

NODE_MODULE(olm, InitAll)
