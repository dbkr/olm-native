/** \brief OlmUtility wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OLM_NAN_UTILITY_HH_
#define __OLM_NAN_UTILITY_HH_

#include "olm/olm.h"

#include <nan.h>

namespace olm_nan {
  class UtilityWrap : public Nan::ObjectWrap {
  public:
    static NAN_MODULE_INIT(Init) {
      v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
      tpl->SetClassName(Nan::New("Utility").ToLocalChecked());
      tpl->InstanceTemplate()->SetInternalFieldCount(1);

      Nan::SetPrototypeMethod(tpl, "free", free);
      Nan::SetPrototypeMethod(tpl, "sha256", sha256);
      Nan::SetPrototypeMethod(tpl, "ed25519_verify", ed25519_verify);

      constructor().Reset(Nan::GetFunction(tpl).ToLocalChecked());
      Nan::Set(target, Nan::New("Utility").ToLocalChecked(),
               Nan::GetFunction(tpl).ToLocalChecked());
    }

  private:
    void *buf;
    OlmUtility *ptr;

    UtilityWrap();
    ~UtilityWrap();

    static inline Nan::Persistent<v8::Function> & constructor() {
      static Nan::Persistent<v8::Function> my_constructor;
      return my_constructor;
    }

    static NAN_METHOD(New);
    static NAN_METHOD(free);
    static NAN_METHOD(sha256);
    static NAN_METHOD(ed25519_verify);
  };
}

#endif // __OLM_NAN_UTILITY_HH_
