{
  "targets": [
    {
      "target_name": "olm",
      "sources": [
        "account.cc",
        "session.cc",
        "utility.cc",
        "inbound_group_session.cc",
        "outbound_group_session.cc",
        "util.cc",
        "olm.cc"
      ],
      "include_dirs": ["<!(node -e \"require('nan')\")"],
      "ldflags": ["-lolm"]
    }
  ]
}
