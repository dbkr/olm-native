/** \brief OlmInboundGroupSession wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "inbound_group_session.hh"
#include "util.hh"

using namespace olm_nan;

InboundGroupSessionWrap::InboundGroupSessionWrap()
  : buf(std::malloc(olm_inbound_group_session_size())),
    ptr(olm_inbound_group_session(buf)) {}

InboundGroupSessionWrap::~InboundGroupSessionWrap() {
  if (ptr) {
    olm_clear_inbound_group_session(ptr);
    std::free(buf);
  }
}

#define CHK_IGSESSION_ERR(x)                                    \
  if (x == olm_error()) {                                        \
    std::string error = "OLM.";                                 \
    error += olm_inbound_group_session_last_error(session);     \
    Nan::ThrowError(error.c_str());                             \
    return;                                                     \
  }

NAN_METHOD(InboundGroupSessionWrap::New) {
  if (info.IsConstructCall()) {
    InboundGroupSessionWrap *wrap = new InboundGroupSessionWrap();
    wrap->Wrap(info.This());
    RETURN(info.This());
  } else {
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = {};
    v8::Local<v8::Function> cons = Nan::New(constructor());
    RETURN(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(InboundGroupSessionWrap::free) {
  InboundGroupSessionWrap* wrap = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder());
  if (wrap->ptr) {
    olm_clear_inbound_group_session(wrap->ptr);
    std::free(wrap->buf);
    wrap->ptr = nullptr;
    wrap->buf = nullptr;
  }
}

NAN_METHOD(InboundGroupSessionWrap::pickle) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));
  size_t pickle_length = olm_pickle_inbound_group_session_length(session);
  Buffer pickle_buffer(pickle_length);
  CHK_IGSESSION_ERR(olm_pickle_inbound_group_session(
    session, key_buffer.get(), key_buffer.length(), pickle_buffer.get(), pickle_length
  ));
  RETURN_STR(pickle_buffer.get(), pickle_length);
}

NAN_METHOD(InboundGroupSessionWrap::unpickle) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));
  Buffer pickle_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked()));
  CHK_IGSESSION_ERR(olm_unpickle_inbound_group_session(
    session, key_buffer.get(), key_buffer.length(),
    pickle_buffer.get(), pickle_buffer.length()
  ));
}

NAN_METHOD(InboundGroupSessionWrap::create) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));

  CHK_IGSESSION_ERR(olm_init_inbound_group_session(
    session, (uint8_t *) key_buffer.get(), key_buffer.length()
  ));
}

NAN_METHOD(InboundGroupSessionWrap::import_session) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));

  CHK_IGSESSION_ERR(olm_import_inbound_group_session(
    session, (uint8_t *) key_buffer.get(), key_buffer.length()
  ));
}

NAN_METHOD(InboundGroupSessionWrap::decrypt) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;

  Buffer message_buffer_tmp(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));

  size_t max_plaintext_length = olm_group_decrypt_max_plaintext_length(
    session, (uint8_t *) message_buffer_tmp.get(), message_buffer_tmp.length()
  );
  CHK_IGSESSION_ERR(max_plaintext_length);

  // caculating the length destroys the input buffer, so we need to re-copy it.
  Buffer message_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));

  Buffer plaintext_buffer(max_plaintext_length);
  uint32_t message_index;

  size_t plaintext_length = olm_group_decrypt(
    session,
    (uint8_t *) message_buffer.get(), message_buffer.length(),
    (uint8_t *) plaintext_buffer.get(), max_plaintext_length,
    &message_index
  );
  CHK_IGSESSION_ERR(plaintext_length);

  v8::Local<v8::Object> rv = Nan::New<v8::Object>();
  Nan::Set(rv, Nan::New("plaintext").ToLocalChecked(), Nan::Encode(plaintext_buffer.get(), plaintext_length, Nan::UTF8));
  Nan::Set(rv, Nan::New("message_index").ToLocalChecked(), Nan::New((uint32_t) message_index));
  RETURN(rv);
}

NAN_METHOD(InboundGroupSessionWrap::session_id) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  size_t length = olm_inbound_group_session_id_length(session);
  Buffer session_id(length);
  CHK_IGSESSION_ERR(olm_inbound_group_session_id(session, (uint8_t *) session_id.get(), length));
  RETURN_STR(session_id.get(), length);
}

NAN_METHOD(InboundGroupSessionWrap::first_known_index) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  RETURN(olm_inbound_group_session_first_known_index(session));
}

NAN_METHOD(InboundGroupSessionWrap::export_session) {
  OlmInboundGroupSession* session = Nan::ObjectWrap::Unwrap<InboundGroupSessionWrap>(info.Holder())->ptr;
  uint32_t message_index = Nan::To<uint32_t>(info[0]).FromJust();
  size_t key_length = olm_export_inbound_group_session_length(session);
  Buffer key(key_length);
  CHK_IGSESSION_ERR(olm_export_inbound_group_session(
    session, (uint8_t *) key.get(), key_length, message_index
  ));
  RETURN_STR(key.get(), key_length);
}
