/** \brief OlmOutboundGroupSession wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OLM_NAN_OUTBOUND_GROUP_SESSION_HH_
#define __OLM_NAN_OUTBOUND_GROUP_SESSION_HH_

#include "olm/olm.h"

#include <nan.h>

namespace olm_nan {

  class OutboundGroupSessionWrap : public Nan::ObjectWrap {
  public:
    static NAN_MODULE_INIT(Init) {
      v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
      tpl->SetClassName(Nan::New("OutboundGroupSession").ToLocalChecked());
      tpl->InstanceTemplate()->SetInternalFieldCount(1);

      Nan::SetPrototypeMethod(tpl, "free", free);
      Nan::SetPrototypeMethod(tpl, "pickle", pickle);
      Nan::SetPrototypeMethod(tpl, "unpickle", unpickle);
      Nan::SetPrototypeMethod(tpl, "create", create);
      Nan::SetPrototypeMethod(tpl, "encrypt", encrypt);
      Nan::SetPrototypeMethod(tpl, "session_id", session_id);
      Nan::SetPrototypeMethod(tpl, "session_key", session_key);
      Nan::SetPrototypeMethod(tpl, "message_index", message_index);

      constructor().Reset(Nan::GetFunction(tpl).ToLocalChecked());
      Nan::Set(target, Nan::New("OutboundGroupSession").ToLocalChecked(),
               Nan::GetFunction(tpl).ToLocalChecked());
    }

  private:
    void *buf;
    OlmOutboundGroupSession *ptr;

    OutboundGroupSessionWrap();
    ~OutboundGroupSessionWrap();

    static inline Nan::Persistent<v8::Function> & constructor() {
      static Nan::Persistent<v8::Function> my_constructor;
      return my_constructor;
    }

    static NAN_METHOD(New);
    static NAN_METHOD(free);
    static NAN_METHOD(pickle);
    static NAN_METHOD(unpickle);
    static NAN_METHOD(create);
    static NAN_METHOD(encrypt);
    static NAN_METHOD(session_id);
    static NAN_METHOD(session_key);
    static NAN_METHOD(message_index);
  };
}

#endif // __OLM_NAN_OUTBOUND_GROUP_SESSION_HH_
