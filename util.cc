/** \brief utility functions
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "util.hh"
#include <cstdlib>
#include <cstring>

#include <openssl/rand.h>
#include <openssl/err.h>

using namespace olm_nan;

// the following function is from Node.js, src/node_crypto.cc
//
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
inline void CheckEntropy() {
  for (;;) {
    int status = RAND_status();
    //CHECK_GE(status, 0);  // Cannot fail.
    if (status != 0)
      break;

    // Give up, RAND_poll() not supported.
    if (RAND_poll() == 0)
      break;
  }
}

Buffer olm_nan::get_random(std::size_t len) {
  Buffer buf(len);
  CheckEntropy();
  if (RAND_bytes((unsigned char*) buf.get(), len) <= 0) {
    std::string error = "Error generating random bytes: ";
    error += ERR_error_string(ERR_get_error(), NULL);
    Nan::ThrowError(error.c_str());
  };
  return buf;
}

Buffer olm_nan::buffer_from_v8_string(const v8::Local<v8::String> &string, Nan::Encoding encoding) {
  auto len = Nan::DecodeBytes(string, encoding);
  Buffer buf(len);
  Nan::DecodeWrite(buf.get(), len, string, encoding);
  return buf;
}

Buffer::~Buffer() {
  if (buf) {
    std::memset(buf.get(), 0, _length);
  }
}
