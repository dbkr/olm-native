/** \brief OlmOutboundGroupSession wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "outbound_group_session.hh"
#include "util.hh"

using namespace olm_nan;

OutboundGroupSessionWrap::OutboundGroupSessionWrap()
  : buf(std::malloc(olm_outbound_group_session_size())),
    ptr(olm_outbound_group_session(buf)) {}

OutboundGroupSessionWrap::~OutboundGroupSessionWrap() {
  if (ptr) {
    olm_clear_outbound_group_session(ptr);
    std::free(buf);
  }
}

#define CHK_OGSESSION_ERR(x)                                    \
  if (x == olm_error()) {                                        \
    std::string error = "OLM.";                                 \
    error += olm_outbound_group_session_last_error(session);     \
    Nan::ThrowError(error.c_str());                             \
    return;                                                     \
  }

NAN_METHOD(OutboundGroupSessionWrap::New) {
  if (info.IsConstructCall()) {
    OutboundGroupSessionWrap *wrap = new OutboundGroupSessionWrap();
    wrap->Wrap(info.This());
    RETURN(info.This());
  } else {
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = {};
    v8::Local<v8::Function> cons = Nan::New(constructor());
    RETURN(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(OutboundGroupSessionWrap::free) {
  OutboundGroupSessionWrap* wrap = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder());
  if (wrap->ptr) {
    olm_clear_outbound_group_session(wrap->ptr);
    std::free(wrap->buf);
    wrap->ptr = nullptr;
    wrap->buf = nullptr;
  }
}

NAN_METHOD(OutboundGroupSessionWrap::pickle) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));
  size_t pickle_length = olm_pickle_outbound_group_session_length(session);
  Buffer pickle_buffer(pickle_length);
  CHK_OGSESSION_ERR(olm_pickle_outbound_group_session(
    session, key_buffer.get(), key_buffer.length(), pickle_buffer.get(), pickle_length
  ));
  RETURN_STR(pickle_buffer.get(), pickle_length);
}

NAN_METHOD(OutboundGroupSessionWrap::unpickle) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  Buffer key_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked()));
  Buffer pickle_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked()));
  CHK_OGSESSION_ERR(olm_unpickle_outbound_group_session(
    session, key_buffer.get(), key_buffer.length(),
    pickle_buffer.get(), pickle_buffer.length()
  ));
}

NAN_METHOD(OutboundGroupSessionWrap::create) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  size_t random_length = olm_init_outbound_group_session_random_length(session);
  Buffer random(get_random(random_length));

  CHK_OGSESSION_ERR(olm_init_outbound_group_session(
    session, (uint8_t *) random.get(), random_length
  ));
}

NAN_METHOD(OutboundGroupSessionWrap::encrypt) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;

  Buffer plaintext_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked(), Nan::UTF8));

  size_t message_length = olm_group_encrypt_message_length(session, plaintext_buffer.length());
  CHK_OGSESSION_ERR(message_length);

  Buffer message_buffer(message_length);

  CHK_OGSESSION_ERR(olm_group_encrypt(
    session,
    (uint8_t *) plaintext_buffer.get(), plaintext_buffer.length(),
    (uint8_t *) message_buffer.get(), message_length
  ));

  RETURN_STR(message_buffer.get(), message_length);
}

NAN_METHOD(OutboundGroupSessionWrap::session_id) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  size_t length = olm_outbound_group_session_id_length(session);
  Buffer session_id(length);
  CHK_OGSESSION_ERR(olm_outbound_group_session_id(session, (uint8_t *) session_id.get(), length));
  RETURN_STR(session_id.get(), length);
}

NAN_METHOD(OutboundGroupSessionWrap::session_key) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  size_t key_length = olm_outbound_group_session_key_length(session);
  Buffer key(key_length);
  CHK_OGSESSION_ERR(olm_outbound_group_session_key(
    session, (uint8_t *) key.get(), key_length
  ));
  RETURN_STR(key.get(), key_length);
}

NAN_METHOD(OutboundGroupSessionWrap::message_index) {
  OlmOutboundGroupSession* session = Nan::ObjectWrap::Unwrap<OutboundGroupSessionWrap>(info.Holder())->ptr;
  RETURN(olm_outbound_group_session_message_index(session));
}
