/** \brief OlmUtility wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "utility.hh"
#include "util.hh"

using namespace olm_nan;

UtilityWrap::UtilityWrap()
  : buf(std::malloc(olm_utility_size())),
    ptr(olm_utility(buf)) {}

UtilityWrap::~UtilityWrap() {
  if (ptr) {
    olm_clear_utility(ptr);
    std::free(buf);
  }
}

#define CHK_UTILITY_ERR(x)                        \
  if (x == olm_error()) {                          \
    std::string error = "OLM.";                   \
    error += olm_utility_last_error(utility);     \
    Nan::ThrowError(error.c_str());               \
    return;                                       \
  }

NAN_METHOD(UtilityWrap::New) {
  if (info.IsConstructCall()) {
    UtilityWrap *wrap = new UtilityWrap();
    wrap->Wrap(info.This());
    RETURN(info.This());
  } else {
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = {};
    v8::Local<v8::Function> cons = Nan::New(constructor());
    RETURN(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(UtilityWrap::free) {
  UtilityWrap* wrap = Nan::ObjectWrap::Unwrap<UtilityWrap>(info.Holder());
  if (wrap->ptr) {
    olm_clear_utility(wrap->ptr);
    std::free(wrap->buf);
    wrap->ptr = nullptr;
    wrap->buf = nullptr;
  }
}

NAN_METHOD(UtilityWrap::sha256) {
  OlmUtility* utility = Nan::ObjectWrap::Unwrap<UtilityWrap>(info.Holder())->ptr;
  size_t output_length = olm_sha256_length(utility);
  Buffer input_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer output_buffer(output_length);
  CHK_UTILITY_ERR(olm_sha256(
    utility,
    input_buffer.get(), input_buffer.length(),
    output_buffer.get(), output_length
  ));
  RETURN_STR(output_buffer.get(), output_length);
}

NAN_METHOD(UtilityWrap::ed25519_verify) {
  OlmUtility* utility = Nan::ObjectWrap::Unwrap<UtilityWrap>(info.Holder())->ptr;
  Buffer key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer message_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  Buffer signature_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[2]).ToLocalChecked());
  CHK_UTILITY_ERR(olm_ed25519_verify(
    utility,
    key_buffer.get(), key_buffer.length(),
    message_buffer.get(), message_buffer.length(),
    signature_buffer.get(), signature_buffer.length()
  ));
}
