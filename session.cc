/** \brief OlmSession wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "account.hh"
#include "session.hh"
#include "util.hh"

using namespace olm_nan;

SessionWrap::SessionWrap()
  : buf(std::malloc(olm_session_size())),
    ptr(olm_session(buf)) {}

SessionWrap::~SessionWrap() {
  if (ptr) {
    olm_clear_session(ptr);
    std::free(buf);
  }
}

#define CHK_SESSION_ERR(x)                        \
  if (x == olm_error()) {                          \
    std::string error = "OLM.";                   \
    error += olm_session_last_error(session);     \
    Nan::ThrowError(error.c_str());               \
    return;                                       \
  }

NAN_METHOD(SessionWrap::New) {
  if (info.IsConstructCall()) {
    SessionWrap *wrap = new SessionWrap();
    wrap->Wrap(info.This());
    RETURN(info.This());
  } else {
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = {};
    v8::Local<v8::Function> cons = Nan::New(constructor());
    RETURN(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(SessionWrap::free) {
  SessionWrap* wrap = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder());
  if (wrap->ptr) {
    olm_clear_session(wrap->ptr);
    std::free(wrap->buf);
    wrap->ptr = nullptr;
    wrap->buf = nullptr;
  }
}

NAN_METHOD(SessionWrap::pickle) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  size_t pickle_length = olm_pickle_session_length(session);
  Buffer key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer pickle_buffer(pickle_length);
  CHK_SESSION_ERR(olm_pickle_session(
    session, key_buffer.get(), key_buffer.length(), pickle_buffer.get(), pickle_length
  ));
  RETURN_STR(pickle_buffer.get(), pickle_buffer.length());
}

NAN_METHOD(SessionWrap::unpickle) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  Buffer key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer pickle_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  CHK_SESSION_ERR(olm_unpickle_session(
    session, key_buffer.get(), key_buffer.length(),
    pickle_buffer.get(), pickle_buffer.length()
  ));
}

NAN_METHOD(SessionWrap::create_outbound) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(Nan::To<v8::Object>(info[0]).ToLocalChecked())->ptr;

  std::size_t random_length = olm_create_outbound_session_random_length(session);
  Buffer random(get_random(random_length));
  Buffer identity_key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  Buffer one_time_key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[2]).ToLocalChecked());
  CHK_SESSION_ERR(olm_create_outbound_session(
    session, account,
    identity_key_buffer.get(), identity_key_buffer.length(),
    one_time_key_buffer.get(), one_time_key_buffer.length(),
    random.get(), random_length
  ));
}

NAN_METHOD(SessionWrap::create_inbound) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(Nan::To<v8::Object>(info[0]).ToLocalChecked())->ptr;

  Buffer message_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  CHK_SESSION_ERR(olm_create_inbound_session(
    session, account, message_buffer.get(), message_buffer.length()
  ));
}

NAN_METHOD(SessionWrap::session_id) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  size_t id_length = olm_session_id_length(session);
  Buffer id_buffer(id_length);
  CHK_SESSION_ERR(olm_session_id(
    session, id_buffer.get(), id_length
  ));
  RETURN_STR(id_buffer.get(), id_length);
}

NAN_METHOD(SessionWrap::has_received_message) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  RETURN(olm_session_has_received_message(session) ? true : false);
}

NAN_METHOD(SessionWrap::matches_inbound) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  Buffer message_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  auto matches_inbound_session = olm_matches_inbound_session(
    session, message_buffer.get(), message_buffer.length()
  );
  CHK_SESSION_ERR(matches_inbound_session);
  RETURN(matches_inbound_session ? true : false);
}

NAN_METHOD(SessionWrap::matches_inbound_from) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  Buffer identity_key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer message_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  auto matches_inbound_session = olm_matches_inbound_session_from(
    session,
    identity_key_buffer.get(), identity_key_buffer.length(),
    message_buffer.get(), message_buffer.length()
  );
  CHK_SESSION_ERR(matches_inbound_session);
  RETURN(matches_inbound_session ? true : false);
}

NAN_METHOD(SessionWrap::encrypt) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;
  size_t random_length = olm_encrypt_random_length(session);
  size_t message_type = olm_encrypt_message_type(session);
  CHK_SESSION_ERR(message_type);
  Buffer plaintext_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked(), Nan::UTF8));
  auto message_length = olm_encrypt_message_length(session, plaintext_buffer.length());

  Buffer random(get_random(random_length));

  Buffer message_buffer(message_length);

  CHK_SESSION_ERR(olm_encrypt(
    session,
    plaintext_buffer.get(), plaintext_buffer.length(),
    random.get(), random_length,
    message_buffer.get(), message_length
  ));

  v8::Local<v8::Object> rv = Nan::New<v8::Object>();
  Nan::Set(rv, Nan::New("type").ToLocalChecked(), Nan::New((int32_t) message_type));
  Nan::Set(rv, Nan::New("body").ToLocalChecked(), Nan::Encode(message_buffer.get(), message_buffer.length(), Nan::UTF8));
  RETURN(rv);
}

NAN_METHOD(SessionWrap::decrypt) {
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(info.Holder())->ptr;

  size_t message_type = (size_t) Nan::To<int32_t>(info[0]).FromJust();
  Buffer message_buffer_tmp(buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked()));

  size_t max_plaintext_length = olm_decrypt_max_plaintext_length(
    session, message_type, message_buffer_tmp.get(), message_buffer_tmp.length()
  );
  CHK_SESSION_ERR(max_plaintext_length);

  // caculating the length destroys the input buffer, so we need to re-copy it
  Buffer message_buffer(buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked()));

  Buffer plaintext_buffer(max_plaintext_length);

  size_t plaintext_length = olm_decrypt(
    session, message_type,
    message_buffer.get(), message_buffer.length(),
    plaintext_buffer.get(), max_plaintext_length
  );
  CHK_SESSION_ERR(plaintext_length);

  RETURN(Nan::Encode(plaintext_buffer.get(), plaintext_length, Nan::UTF8));
}
